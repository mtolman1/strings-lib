#include "../src/data_structures.h"
#include "doctest.h"

TEST_SUITE("Trie") {
  TEST_CASE("Simple Trie") {
    auto t = mst::patterns::data_struct::trie(std::string{"abcdefg"});
    CHECK(t.contains("abcdefg"));
    CHECK(!t.contains("abcdefgh"));
    CHECK(!t.contains("abcde"));
    CHECK(!t.contains("bd3"));
    CHECK(!t.contains(""));
  }

  TEST_CASE("Multiple Strings") {
    auto t = mst::patterns::data_struct::trie("abcdefg");
    t.add("bhikj");
    t.add("bh");
    t.add(std::string{"abcd"});
    t.add("");
    CHECK(t.contains("abcdefg"));
    CHECK(t.contains("abcd"));
    CHECK(t.contains("bhikj"));
    CHECK(t.contains(""));
    CHECK(t.contains(std::string{"bh"}));
    CHECK(!t.contains("abcdefgh"));
    CHECK(!t.contains("abcde"));
    CHECK(!t.contains("bd3"));
  }
}
