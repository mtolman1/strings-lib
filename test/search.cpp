#include "../src/search.h"
#include "doctest.h"
#include <vector>

#define TestSearch(NAME, FUNC)                                                                                        \
  TEST_SUITE(NAME) {                                                                                                  \
    TEST_CASE("Strings") {                                                                                            \
      std::string abcd = "abcd";                                                                                      \
                                                                                                                      \
      SUBCASE("Begin") {                                                                                              \
        auto resIt = mst::patterns::match::range::FUNC("abc", abcd);                                                  \
        CHECK_EQ(*resIt.begin(), abcd.begin());                                                                        \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Mid") {                                                                                                \
        auto resIt = mst::patterns::match::range::FUNC("bc", abcd);                                                   \
        CHECK_EQ(*resIt.begin(), abcd.begin() + 1);                                                                    \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("End") {                                                                                                \
        auto resIt = mst::patterns::match::range::FUNC("d", abcd);                                                    \
        CHECK_EQ(*resIt.begin(), abcd.begin() + 3);                                                                   \
        CHECK_NE(resIt.begin(), resIt.end());                                                                         \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Less Than") {                                                                                          \
        auto resIt = mst::patterns::match::range::FUNC<std::less<>>("bcd", abcd);                                     \
        CHECK_EQ(*resIt.begin(), abcd.begin());                                                                        \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Invalid") {                                                                                            \
        auto resIt = mst::patterns::match::range::FUNC("dc", abcd);                                                   \
        CHECK_EQ(*resIt.begin(), abcd.end());                                                                         \
        CHECK_EQ(resIt.begin(), resIt.end());                                                                         \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Multi") {                                                                                              \
        auto str = std::string{"abc abc abc"};                                                                                                              \
        auto resIt = mst::patterns::match::range::FUNC("abc", str);                            \
        int expectedOffset = 0;                                                                                       \
        int expectedCount = 3;                                                                                        \
        int count = 0;         \
        for (auto &it: resIt) { \
          CHECK_EQ(it, str.begin() + expectedOffset); \
          expectedOffset += 4; \
          *it = 'c'; \
          ++count; \
        }                                                                             \
        CHECK_EQ(count, expectedCount);                                                                               \
        CHECK_EQ(str, "cbc cbc cbc");                                                                                                              \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Curry") {                                                                                              \
        auto matcher = mst::patterns::match::curry::FUNC("dc");                                                       \
        std::string one = "abc";                                                                                      \
        std::string two = "dce";                                                                                      \
        CHECK_EQ(*matcher(one).begin(), one.end());                                                                    \
        CHECK_EQ(*matcher(two).begin(), two.begin());                                                               \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Different Types") {                                                                                    \
        auto resIt = mst::patterns::match::range::FUNC(std::vector<char>{'a', 'b', 'c'}, std::string{"abc abc abc"}); \
        int expectedOffset = 0;                                                                                       \
        int expectedCount = 3;                                                                                        \
        int count = 0;                                                                                                \
        for (const auto &it: resIt) {                                                                                 \
          ++count;                                                                                                    \
        }                                                                                                             \
        CHECK_EQ(count, expectedCount);                                                                               \
      }                                                                                                               \
    }                                                                                                                 \
                                                                                                                      \
    TEST_CASE("Vector") {                                                                                             \
      SUBCASE("Begin") {                                                                                              \
        std::vector<int> base{1, 2, 3, 4, 5};                                                                         \
        auto resIt = mst::patterns::match::range::FUNC(decltype(base){1, 2, 3}, base);                                \
        const auto begin = resIt.begin();                                                                             \
        const auto expected = base.begin();                                                                           \
        CHECK_EQ(*begin, expected);                                                                                    \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Mid") {                                                                                                \
        std::vector<int> base{1, 2, 3, 4, 5};                                                                         \
        auto resIt = mst::patterns::match::range::FUNC(decltype(base){3, 4}, base);                                   \
        CHECK_EQ(*resIt.begin(), base.begin() + 2);                                                                    \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("End") {                                                                                                \
        std::vector<int> base{1, 2, 3, 4, 5};                                                                         \
        auto resIt = mst::patterns::match::range::FUNC(decltype(base){5}, base);                                      \
        CHECK_EQ(*resIt.begin(), base.begin() + 4);                                                                    \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Invalid") {                                                                                            \
        std::vector<int> base{1, 2, 3, 4, 5};                                                                         \
        auto resIt = mst::patterns::match::range::FUNC(decltype(base){4, 5, 6}, base);                                \
        CHECK_EQ(*resIt.begin(), base.end());                                                                          \
      }                                                                                                               \
                                                                                                                      \
      SUBCASE("Curry") {                                                                                              \
        std::vector<int> one = {2, 3, 4};                                                                             \
        std::vector<int> two = {1, 2, 3};                                                                             \
        auto matcher = mst::patterns::match::curry::FUNC(decltype(one){1, 2});                                        \
        CHECK_EQ(*matcher(one).begin(), one.end());                                                                    \
        CHECK_EQ(*matcher(two).begin(), two.begin());                                                                  \
      }                                                                                                               \
    }                                                                                                                 \
  }

TestSearch("Naive", naive);
TestSearch("Border Array", borderArray);
TestSearch("Knuth Morris Pratt", knuthMorrisPratt);

TEST_SUITE("Test 2") {
  TEST_CASE("Strings") {
    std::string abcd = "abcd";

    SUBCASE("Begin") {
      auto resIt = mst::patterns::match::range::borderArray("abc", abcd).begin();
      auto it = *resIt;
      auto val = *it;
      CHECK_EQ(*resIt, abcd.begin());
    }

    SUBCASE("Mid") {
      auto resIt = mst::patterns::match::range::naive("bc", abcd);
      CHECK_EQ(*resIt.begin(), abcd.begin() + 1);
    }

    SUBCASE("End") {
      auto resIt = mst::patterns::match::range::naive("d", abcd);
      CHECK_EQ(*resIt.begin(), abcd.begin() + 3);
    }

    SUBCASE("Less Than") {
      auto resIt = mst::patterns::match::range::naive<std::less<>>("bcd", abcd);
      CHECK_EQ(*resIt.begin(), abcd.begin());
    }

    SUBCASE("Invalid") {
      auto resIt = mst::patterns::match::range::naive("dc", abcd);
      CHECK_EQ(*resIt.begin(), abcd.end());
    }

    SUBCASE("Multi") {
      auto str = std::string{"abc abc abc"};
      auto resIt = mst::patterns::match::range::borderArray("abc", str);
      int expectedOffset = 0;
      int expectedCount = 3;
      int count = 0;
      for (auto &it: resIt) {
        CHECK_EQ(it, str.begin() + expectedOffset);
        expectedOffset += 4;
        *it = 'c';
        ++count;
      }

      CHECK_EQ(count, expectedCount);
      CHECK_EQ(str, "cbc cbc cbc");
    }

    SUBCASE("Curry") {
      auto matcher = mst::patterns::match::curry::naive("dc");
      std::string one = "abc";
      std::string two = "dce";
      CHECK_EQ(*matcher(one).begin(), one.end());
      CHECK_EQ((*matcher(two).begin()), two.begin());
      CHECK_EQ(*matcher(two).begin(), two.begin());
    }

    SUBCASE("Different Types") {
      auto resIt = mst::patterns::match::range::borderArray(std::vector<char>{'a', 'b', 'c'}, std::string{"abc abc abc"});
      int expectedOffset = 0;
      int expectedCount = 3;
      int count = 0;
      for (const auto &it: resIt) {
        ++count;
      }
      CHECK_EQ(count, expectedCount);
    }
  }

  TEST_CASE("Vector") {
    SUBCASE("Begin") {
      std::vector<int> base{1, 2, 3, 4, 5};
      auto resIt = mst::patterns::match::range::naive(decltype(base){1, 2, 3}, base);
      const auto begin = resIt.begin();
      const auto expected = base.begin();
      CHECK_EQ(*begin, expected);
    }

    SUBCASE("Mid") {
      std::vector<int> base{1, 2, 3, 4, 5};
      auto resIt = mst::patterns::match::range::naive(decltype(base){3, 4}, base);
      CHECK_EQ(*resIt.begin(), base.begin() + 2);
    }

    SUBCASE("End") {
      std::vector<int> base{1, 2, 3, 4, 5};
      auto resIt = mst::patterns::match::range::naive(decltype(base){5}, base);
      CHECK_EQ(*resIt.begin(), base.begin() + 4);
    }

    SUBCASE("Invalid") {
      std::vector<int> base{1, 2, 3, 4, 5};
      auto resIt = mst::patterns::match::range::naive(decltype(base){4, 5, 6}, base);
      CHECK_EQ(*resIt.begin(), base.end());
    }

    SUBCASE("Curry") {
      std::vector<int> one = {2, 3, 4};
      std::vector<int> two = {1, 2, 3};
      auto matcher = mst::patterns::match::curry::naive(decltype(one){1, 2});
      CHECK_EQ(*matcher(one).begin(), one.end());
      CHECK_EQ(*matcher(two).begin(), two.begin());
    }
  }
}