#pragma once

#include <cstddef>
#include <functional>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

namespace mst {
  namespace patterns {
    namespace match {
      namespace context {
        namespace details {
          template<typename V, typename T, typename Cmp>
          inline void fillBorderArray(V &borderArray, T &search, const Cmp &comparator) {
            auto sIt = std::begin(search);
            ++sIt;
            size_t i = 1;
            borderArray.template emplace_back(0);
            for (; sIt != std::end(search); ++i, ++sIt) {
              auto b = borderArray[i - 1];
              while (b > 0 && !comparator(search[i], search[b])) {
                b = borderArray[i - 1];
              }
              borderArray.template emplace_back(comparator(search[i], search[b]) ? b + 1 : 0);
            }
          }


          template<bool Include, typename Iter>
          struct Cache {};
          template<typename Iter>
          struct Cache<true, Iter> {
            std::vector<Iter> cache{};
            ;
          };
        }// namespace details

        template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
        class BorderArrayContext {
        public:
          BorderArrayContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackBegin, const Iter &haystackEnd)
              : cacheIt(haystackBegin), haystackBegin(haystackBegin), haystackEnd(haystackEnd) {
            lastSearchIndex = 0;
            searchSize = static_cast<decltype(searchSize)>(std::distance(searchBegin, searchEnd));
            if (!searchSize || haystackBegin == haystackEnd) {
              searchSize = 0;
              return;
            }
            if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
              if (searchSize > std::distance(haystackBegin, haystackEnd)) {
                return;
              }

              borderArray.reserve(std::distance(searchBegin, searchEnd) + 1 + std::max(searchSize, std::distance(haystackBegin, haystackEnd) / 2));
            } else {
              borderArray.reserve(std::distance(searchBegin, searchEnd) * 2 + 1);
            }

            search.resize(searchSize);
            std::copy(searchBegin, searchEnd, std::begin(search));

            details::fillBorderArray(borderArray, search, comparator);
            borderArray.template emplace_back(0);

            if (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
              cacheObj.cache.reserve(searchSize * 2);
            }

            lastSearchIndex = searchSize + 1;
          }

          Iter doStep() {
            if (!searchSize) {
              return haystackEnd;
            }
            for (; cacheIt != haystackEnd; ++lastSearchIndex, ++cacheIt) {
              if (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                cacheObj.cache.template emplace_back(cacheIt);
              }
              auto b = borderArray[lastSearchIndex - 1];
              while (b > 0 && !compareIndices(lastSearchIndex, b)) {
                b = borderArray[b - 1];
              }
              borderArray.template emplace_back(compareIndices(lastSearchIndex, b) ? b + 1 : 0);
              if (borderArray[lastSearchIndex] == searchSize) {
                Iter res;
                if (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  res = cacheObj.cache[lastSearchIndex - searchSize * 2];
                } else {
                  res = haystackBegin + lastSearchIndex - searchSize * 2;
                }
                // Increment so we come back in the next loop iteration
                ++lastSearchIndex;
                ++cacheIt;
                return res;
              }
            }
            return haystackEnd;
          }

        private:
          const Iter haystackBegin;
          const Iter haystackEnd;
          Iter cacheIt;
          std::vector<int> borderArray{};
          std::vector<typename std::iterator_traits<SIter>::value_type> search{};
          details::Cache<std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value, Iter> cacheObj;
          typename std::iterator_traits<Iter>::difference_type searchSize{};
          cmp comparator{};
          size_t lastSearchIndex{};

          bool compareIndices(size_t leftIndex, size_t rightIndex) const noexcept {
            if (leftIndex < search.size()) {
              if (rightIndex < search.size()) {
                return comparator(search[leftIndex], search[rightIndex]);
              } else if (rightIndex > search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(search[leftIndex], *(haystackBegin + lastSearchIndex - (rightIndex - search.size() - 1)));
                } else {
                  return comparator(search[leftIndex], *cacheObj.cache[lastSearchIndex - (rightIndex - search.size() - 1)]);
                }
              } else {
                return false;
              }
            } else if (leftIndex > search.size()) {
              if (rightIndex < search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(*(haystackBegin + (leftIndex - search.size() - 1)), search[rightIndex]);
                } else {
                  return comparator(cacheObj.cache[leftIndex - search.size() - 1], search[rightIndex]);
                }
              } else if (rightIndex > search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(*(haystackBegin + (leftIndex - search.size() - 1)), *(haystackBegin + (rightIndex - search.size() - 1)));
                } else {
                  return comparator(cacheObj.cache[leftIndex - search.size() - 1], cacheObj.cache[rightIndex - search.size() - 1]);
                }
              } else {
                return false;
              }
            } else {
              return false;
            }
          }
          BorderArrayContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackEnd)
              : haystackEnd(haystackEnd) {
            cacheIt = haystackEnd;
            searchSize = 0;
          }
        };

        template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
        class KnuthMorrisPrattContext {
        public:
          KnuthMorrisPrattContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackBegin, const Iter &haystackEnd)
              : cacheIt(haystackBegin), haystackEnd(haystackEnd) {
            searchSize = std::distance(searchBegin, searchEnd);
            if (!searchSize || cacheIt == haystackEnd) {
              searchSize = 0;
              return;
            }

            if (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
              if (searchSize > std::distance(haystackBegin, haystackEnd)) {
                return;
              }

              borderArray.reserve(std::distance(searchBegin, searchEnd) + 1 + std::max(searchSize, std::distance(haystackBegin, haystackEnd) / 2));
            } else {
              borderArray.reserve(std::distance(searchBegin, searchEnd) * 2 + 1);
            }

            search.resize(searchSize);
            std::copy(searchBegin, searchEnd, std::begin(search));
            details::fillBorderArray(borderArray, search, comparator);

            for (size_t i = 0; i < borderArray.size() - 1; ++i) {
              if (borderArray[i] > 0 && comparator(search[borderArray[i]], search[i + 1])) {
                borderArray[i] = borderArray[borderArray[i] - 1];
              }
            }

            if constexpr (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
              cacheObj.cache.template emplace_back(cacheIt);
            }
          }

          Iter doStep() {
            if (!searchSize || cacheIt == haystackEnd) {
              return haystackEnd;
            }
            while (cacheIt != haystackEnd) {
              while (searchVectorIndex < search.size() && cacheIt != haystackEnd && comparator(*cacheIt, search[searchVectorIndex])) {
                ++lastSearchIndex;
                ++searchVectorIndex;
                ++cacheIt;
                if constexpr (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  cacheObj.cache.template emplace_back(cacheIt);
                }
              }

              // Save our indices; we have to update class instances prior to returning to avoid infinite looping
              auto svi = searchVectorIndex;
              auto lsi = lastSearchIndex;
              auto ci = cacheIt;

              // Update our indices
              if (searchVectorIndex == 0) {
                ++lastSearchIndex;
                ++cacheIt;
                if constexpr (!std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  if (cacheIt != haystackEnd) {
                    cacheObj.cache.template emplace_back(cacheIt);
                  }
                }
              } else {
                searchVectorIndex = borderArray[searchVectorIndex - 1];
              }

              // Use our saved copies to see if we should return
              if (svi == search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return ci - svi;
                } else {
                  return cacheObj.cache[lsi - svi];
                }
              }
            }
            return haystackEnd;
          }

        private:
          const Iter haystackEnd{};
          typename std::iterator_traits<Iter>::difference_type searchSize{};
          Iter cacheIt{};
          std::vector<typename std::iterator_traits<SIter>::value_type> search{};
          size_t lastSearchIndex = 0;
          size_t searchVectorIndex = 0;
          std::vector<int> borderArray{};

          // Cache that will be used when we don't have random access to skip around
          details::Cache<std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value, Iter> cacheObj;
          cmp comparator{};

          bool compareIndices(size_t leftIndex, size_t rightIndex) const noexcept {
            if (leftIndex < search.size()) {
              if (rightIndex < search.size()) {
                return comparator(search[leftIndex], search[rightIndex]);
              } else if (rightIndex > search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(search[leftIndex], *(cacheIt - lastSearchIndex - (rightIndex - search.size() - 1)));
                } else {
                  return comparator(search[leftIndex], *cacheObj.cache[lastSearchIndex - (rightIndex - search.size() - 1)]);
                }
              } else {
                return false;
              }
            } else if (leftIndex > search.size()) {
              if (rightIndex < search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(*(cacheIt - lastSearchIndex - (leftIndex - search.size() - 1)), search[rightIndex]);
                } else {
                  return comparator(cacheObj.cache[leftIndex - search.size() - 1], search[rightIndex]);
                }
              } else if (rightIndex > search.size()) {
                if constexpr (std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
                  return comparator(*(cacheIt - lastSearchIndex - (leftIndex - search.size() - 1)), *(cacheIt - lastSearchIndex - (rightIndex - search.size() - 1)));
                } else {
                  return comparator(cacheObj.cache[leftIndex - search.size() - 1], cacheObj.cache[rightIndex - search.size() - 1]);
                }
              } else {
                return false;
              }
            } else {
              return false;
            }
          }
          KnuthMorrisPrattContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackEnd)
              : haystackEnd(haystackEnd) {
            cacheIt = haystackEnd;
            searchSize = 0;
          }
        };
      }// namespace context

      template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
      Iter naive(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackBegin, const Iter &haystackEnd) {
        auto sD = std::distance(searchBegin, searchEnd);
        if (!sD || haystackBegin == haystackEnd) {
          return haystackEnd;
        }

        if (std::is_same<typename std::iterator_traits<SIter>::iterator_category, std::random_access_iterator_tag>::value &&
            std::is_same<typename std::iterator_traits<Iter>::iterator_category, std::random_access_iterator_tag>::value) {
          if (sD > std::distance(haystackBegin, haystackEnd)) {
            return haystackEnd;
          }
        }
        auto comparator = cmp{};
        for (auto stackIt = haystackBegin; stackIt != haystackEnd; stackIt++) {
          auto searchIt = searchBegin;
          auto stackCheckIt = stackIt;
          for (;
               searchIt != searchEnd && stackCheckIt != haystackEnd && comparator(*stackCheckIt, *searchIt);
               ++stackCheckIt, ++searchIt) {}
          if (searchIt == searchEnd) {
            return stackIt;
          }
        }
        return haystackEnd;
      }

      template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
      context::BorderArrayContext<SIter, Iter, cmp> borderArrayContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackBegin, const Iter &haystackEnd) {
        return {searchBegin, searchEnd, haystackBegin, haystackEnd};
      }

      template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
      Iter borderArray(context::BorderArrayContext<SIter, Iter, cmp> &ctx) {
        return ctx.doStep();
      }

      template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
      context::KnuthMorrisPrattContext<SIter, Iter, cmp> knuthMorrisPrattContext(const SIter &searchBegin, const SIter &searchEnd, const Iter &haystackBegin, const Iter &haystackEnd) {
        return {searchBegin, searchEnd, haystackBegin, haystackEnd};
      }

      template<typename SIter, typename Iter, typename cmp = std::equal_to<>>
      Iter knuthMorrisPratt(context::KnuthMorrisPrattContext<SIter, Iter, cmp> &ctx) {
        return ctx.doStep();
      }

      namespace details {
        template<typename Func, typename SIter, typename Iter>
        struct MatchIter {
          MatchIter(Func func, SIter searchBegin, SIter searchEnd, Iter haystackBegin, Iter haystackEnd)
              : func(std::move(func)), searchBegin(std::move(searchBegin)), searchEnd(std::move(searchEnd)), haystackEnd(std::move(haystackEnd)) {
            current = std::make_shared<Iter>(this->func(this->searchBegin, this->searchEnd, haystackBegin, this->haystackEnd));
          }
          MatchIter() = default;
          MatchIter(const MatchIter &other) {
            func = other.func;
            searchBegin = other.searchBegin;
            searchEnd = other.searchEnd;
            current = other.current;
            searchEnd = other.searchEnd;
          };
          MatchIter(MatchIter &&other) noexcept
              : func(other.func), searchBegin(std::move(other.searchBegin)), searchEnd(std::move(other.searchEnd)), current(std::move(other.current)), haystackEnd(other.haystackEnd) {}

          MatchIter &operator=(const MatchIter &other) = default;
          MatchIter &operator=(MatchIter &&other) noexcept {
            std::swap(func, other.func);
            std::swap(searchBegin, other.searchBegin);
            std::swap(searchEnd, other.searchEnd);
            std::swap(current, other.current);
            std::swap(haystackEnd, other.haystackEnd);
          }

          using iterator_category = std::forward_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = Iter;
          using pointer = value_type *;
          using reference = value_type &;

          reference operator*() const { return *current; }
          pointer operator->() const { return &*current; }

          // Prefix increment
          MatchIter &operator++() {
            current = std::make_shared<Iter>(std::move(func(searchBegin, searchEnd, ++(*current), haystackEnd)));
            return *this;
          }

          // Postfix increment
          MatchIter operator++(int) {
            MatchIter tmp = *this;
            ++(*this);
            return tmp;
          }

          using DIter = typename std::decay<Iter>::type;
          operator Iter() const { return *current; }

          friend bool operator==(const MatchIter &a, const MatchIter &b) { return (*a.current) == (*b.current); };
          friend bool operator!=(const MatchIter &a, const MatchIter &b) { return !(a == b); };

        private:
          Func func;
          SIter searchBegin;
          SIter searchEnd;
          Iter haystackEnd;
          std::shared_ptr<Iter> current;
        };

        template<typename Func, typename Iter, typename Context>
        struct MatchIterContext {
          MatchIterContext(Func func, Context ctx)
              : func(std::move(func)), ctx(std::move(ctx)) {
            current = std::make_shared<Iter>(this->func(this->ctx));
          }
          MatchIterContext() = default;
          MatchIterContext(const MatchIterContext &other) {
            func = other.func;
            ctx = other.ctx;
            current = other.current;
          };
          MatchIterContext(MatchIterContext &&other) noexcept
              : func(other.func), ctx(std::move(other.ctx)), current(std::move(other.current)) {}

          MatchIterContext &operator=(const MatchIterContext &other) = default;
          MatchIterContext &operator=(MatchIterContext &&other) noexcept {
            std::swap(func, other.func);
            std::swap(ctx, other.ctx);
            std::swap(current, other.current);
          }

          using iterator_category = std::forward_iterator_tag;
          using difference_type = std::ptrdiff_t;
          using value_type = Iter;
          using pointer = value_type *;
          using reference = value_type &;

          reference operator*() const { return *current; }
          pointer operator->() const { return &*current; }

          // Prefix increment
          MatchIterContext &operator++() {
            current = std::make_shared<Iter>(func(ctx));
            return *this;
          }

          // Postfix increment
          MatchIterContext operator++(int) {
            MatchIterContext tmp = *this;
            ++(*this);
            return tmp;
          }

          using DIter = typename std::decay<Iter>::type;
          operator Iter() const { return *current; }

          friend bool operator==(const MatchIterContext &a, const MatchIterContext &b) { return (*a.current) == (*b.current); };
          friend bool operator!=(const MatchIterContext &a, const MatchIterContext &b) { return !(a == b); };

        private:
          Func func;
          Context ctx;
          std::shared_ptr<Iter> current;
        };

        template<typename Func, typename SIter, typename Iter>
        auto matchIter(Func func, SIter searchBegin, SIter searchEnd, Iter haystackBegin, Iter haystackEnd) {
          return MatchIter<Func, SIter, Iter>(std::move(func), std::move(searchBegin), std::move(searchEnd), std::move(haystackBegin), std::move(haystackEnd));
        }

        template<typename Func, typename Context>
        auto matchIter(Func func, Context context) {
          return MatchIterContext<Func, decltype(func(context)), Context>(std::move(func), std::move(context));
        }
      }// namespace details

      namespace range {
        template<typename Func, typename T, typename G>
        struct MatchRangeContain {
          MatchRangeContain(Func func, T search, G haystack)
              : func(std::move(func)), search(std::move(search)), haystack(std::move(haystack)) {}

          MatchRangeContain() = default;
          MatchRangeContain(const MatchRangeContain &other) = default;
          MatchRangeContain(MatchRangeContain &&other) noexcept = default;
          MatchRangeContain &operator=(const MatchRangeContain &other) = default;
          MatchRangeContain &operator=(MatchRangeContain &&other) noexcept = default;

        private:
          Func func;
          const T search;
          const G haystack;

        public:
          using iterator = decltype(details::matchIter(func, std::begin(search), std::end(search), std::begin(haystack), std::end(haystack)));

          auto begin() {
            return details::matchIter(func, std::begin(search), std::end(search), std::begin(haystack), std::end(haystack));
          }

          auto end() {
            return details::matchIter(func, std::end(search), std::end(search), std::end(haystack), std::end(haystack));
          }
        };
        template<typename Func, typename T, typename Iter>
        struct MatchRange {
          MatchRange(Func func, T search, Iter haystackBegin, Iter haystackEnd)
              : func(std::move(func)), search(std::move(search)), haystackBegin(std::move(haystackBegin)), haystackEnd(std::move(haystackEnd)) {}

          MatchRange() = default;
          MatchRange(const MatchRange &other) = default;
          MatchRange(MatchRange &&other) noexcept = default;
          MatchRange &operator=(const MatchRange &other) = default;
          MatchRange &operator=(MatchRange &&other) noexcept = default;

        private:
          Func func;
          const T search;
          Iter haystackBegin;
          Iter haystackEnd;

        public:
          using iterator = decltype(details::matchIter(func, std::begin(search), std::end(search), haystackBegin, haystackEnd));

          auto begin() {
            return details::matchIter(func, std::begin(search), std::end(search), haystackBegin, haystackEnd);
          }

          auto end() {
            return details::matchIter(func, std::end(search), std::end(search), haystackEnd, haystackEnd);
          }
        };

        template<typename FuncContext, typename FuncIter, typename T, typename G>
        struct MatchRangeContainContext {
          MatchRangeContainContext(FuncContext ctxFunc, FuncIter func, T search, G haystack)
              : func(std::move(func)), search(std::move(search)), haystack(std::move(haystack)), contextFunc(std::move(ctxFunc)) {
          }

          MatchRangeContainContext() = default;
          MatchRangeContainContext(const MatchRangeContainContext &other) = default;
          MatchRangeContainContext(MatchRangeContainContext &&other) noexcept = default;
          MatchRangeContainContext &operator=(const MatchRangeContainContext &other) = default;
          MatchRangeContainContext &operator=(MatchRangeContainContext &&other) noexcept = default;

        private:
          FuncIter func;
          const T search;
          const G haystack;
          FuncContext contextFunc;

        public:
          using iterator = decltype(details::matchIter(func, contextFunc(std::begin(search), std::end(search), std::begin(haystack), std::end(haystack))));

          auto begin() {
            return details::matchIter(func, contextFunc(std::begin(search), std::end(search), std::begin(haystack), std::end(haystack)));
          }

          auto end() {
            return details::matchIter(func, contextFunc(std::begin(search), std::end(search), std::end(haystack), std::end(haystack)));
          }
        };

        template<typename FuncContext, typename FuncIter, typename T, typename Iter>
        struct MatchRangeContext {
          MatchRangeContext(FuncContext ctxFunc, FuncIter func, T search, Iter haystackBegin, Iter haystackEnd)
              : func(std::move(func)), search(std::move(search)), haystackBegin(std::move(haystackBegin)), haystackEnd(std::move(haystackEnd)), contextFunc(std::move(ctxFunc)) {}

          MatchRangeContext() = default;
          MatchRangeContext(const MatchRangeContext &other) = default;
          MatchRangeContext(MatchRangeContext &&other) noexcept = default;
          MatchRangeContext &operator=(const MatchRangeContext &other) = default;
          MatchRangeContext &operator=(MatchRangeContext &&other) noexcept = default;

        private:
          FuncIter func;
          const T search;
          Iter haystackBegin;
          Iter haystackEnd;
          FuncContext contextFunc;

        public:
          using iterator = decltype(details::matchIter(func, contextFunc(std::begin(search), std::end(search), haystackBegin, haystackEnd)));

          auto begin() {
            return details::matchIter(func, contextFunc(std::begin(search), std::end(search), haystackBegin, haystackEnd));
          }

          auto end() {
            return details::matchIter(func, contextFunc(std::begin(search), std::end(search), haystackEnd, haystackEnd));
          }
        };

        namespace details {
          template<typename Func, typename T, typename Iter>
          auto matchRange(Func func, T search, Iter haystackBegin, Iter haystackEnd) {
            return MatchRange<Func, T, Iter>(func, std::move(search), haystackBegin, haystackEnd);
          }
          template<typename Func, typename T, typename G>
          auto matchRange(Func func, T search, G &&haystack) {
            return MatchRangeContain<Func, T, G>(func, std::move(search), haystack);
          }

          template<typename ContextFunc, typename Func, typename T, typename Iter>
          auto matchRange(ContextFunc ctxFunc, Func func, T search, Iter haystackBegin, Iter haystackEnd) {
            return MatchRangeContext<ContextFunc, Func, T, Iter>(ctxFunc, func, std::move(search), haystackBegin, haystackEnd);
          }
          template<typename ContextFunc, typename Func, typename T, typename G>
          auto matchRange(ContextFunc ctxFunc, Func func, T search, G &&haystack) {
            return MatchRangeContainContext<ContextFunc, Func, T, G>(ctxFunc, func, std::move(search), haystack);
          }
        }// namespace details
      }  // namespace range


#define WRAP_METHOD_NO_CONTEXT(METHOD)                                                                                                                                         \
  namespace range {                                                                                                                                                            \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                           \
    auto METHOD(T search, G &haystack) {                                                                                                                                       \
      const auto getFunc = [](const T &search, G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; };       \
      return details::matchRange(getFunc(search, haystack), std::move(search), std::begin(haystack), std::end(haystack));                                                      \
    }                                                                                                                                                                          \
                                                                                                                                                                               \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                           \
    auto METHOD(T search, G &&haystack) {                                                                                                                                      \
      const auto getFunc = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; }; \
      return details::matchRange(getFunc(search, haystack), std::move(search), std::move(haystack));                                                                           \
    }                                                                                                                                                                          \
                                                                                                                                                                               \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                           \
    auto METHOD(T search, const G &haystack) {                                                                                                                                 \
      const auto getFunc = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; }; \
      return details::matchRange(getFunc(search, haystack), std::move(search), std::begin(haystack), std::end(haystack));                                                      \
    }                                                                                                                                                                          \
                                                                                                                                                                               \
    template<typename cmp = std::equal_to<>, typename G>                                                                                                                       \
    auto METHOD(const char *search, G &haystack) {                                                                                                                             \
      return METHOD<cmp>(std::string{search}, haystack);                                                                                                                       \
    }                                                                                                                                                                          \
                                                                                                                                                                               \
    template<typename cmp = std::equal_to<>, typename T>                                                                                                                       \
    auto METHOD(T search, const char *haystack) {                                                                                                                              \
      return METHOD<cmp>(std::move(search), std::string{haystack});                                                                                                            \
    }                                                                                                                                                                          \
                                                                                                                                                                               \
    template<typename cmp = std::equal_to<>>                                                                                                                                   \
    auto METHOD(const char *search, const char *haystack) {                                                                                                                    \
      return METHOD<cmp>(std::string{search}, std::string{haystack});                                                                                                          \
    }                                                                                                                                                                          \
  }                                                                                                                                                                            \
                                                                                                                                                                               \
  namespace curry {                                                                                                                                                            \
    template<typename cmp = std::equal_to<>, typename T>                                                                                                                       \
    auto METHOD(T search) {                                                                                                                                                    \
      return [=](auto &haystack) {                                                                                                                                             \
        return range::METHOD<cmp>(search, haystack);                                                                                                                           \
      };                                                                                                                                                                       \
    }                                                                                                                                                                          \
  }

#define WRAP_METHOD_CONTEXT(METHOD)                                                                                                                                                        \
  namespace range {                                                                                                                                                                        \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                                       \
    auto METHOD(T search, G &haystack) {                                                                                                                                                   \
      const auto getFunc = [](const T &search, G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; };                   \
      const auto getContext = [](const T &search, G &haystack) { return mst::patterns::match::METHOD##Context<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; };       \
      return details::matchRange(getContext(search, haystack), getFunc(search, haystack), std::move(search), std::begin(haystack), std::end(haystack));                                    \
    }                                                                                                                                                                                      \
                                                                                                                                                                                           \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                                       \
    auto METHOD(T search, G &&haystack) {                                                                                                                                                  \
      const auto getFunc = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; };             \
      const auto getContext = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD##Context<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; }; \
      return details::matchRange(getContext(search, haystack), getFunc(search, haystack), std::move(search), std::move(haystack));                                                         \
    }                                                                                                                                                                                      \
                                                                                                                                                                                           \
    template<typename cmp = std::equal_to<>, typename T, typename G>                                                                                                                       \
    auto METHOD(T search, const G &haystack) {                                                                                                                                             \
      const auto getFunc = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; };             \
      const auto getContext = [](const T &search, const G &haystack) { return mst::patterns::match::METHOD##Context<decltype(std::begin(search)), decltype(std::begin(haystack)), cmp>; }; \
      return details::matchRange(getContext(search, haystack), getFunc(search, haystack), std::move(search), std::begin(haystack), std::end(haystack));                                    \
    }                                                                                                                                                                                      \
                                                                                                                                                                                           \
    template<typename cmp = std::equal_to<>, typename G>                                                                                                                                   \
    auto METHOD(const char *search, G &haystack) {                                                                                                                                         \
      return METHOD<cmp>(std::string{search}, haystack);                                                                                                                                   \
    }                                                                                                                                                                                      \
                                                                                                                                                                                           \
    template<typename cmp = std::equal_to<>, typename T>                                                                                                                                   \
    auto METHOD(T search, const char *haystack) {                                                                                                                                          \
      return METHOD<cmp>(std::move(search), std::string{haystack});                                                                                                                        \
    }                                                                                                                                                                                      \
                                                                                                                                                                                           \
    template<typename cmp = std::equal_to<>>                                                                                                                                               \
    auto METHOD(const char *search, const char *haystack) {                                                                                                                                \
      return METHOD<cmp>(std::string{search}, std::string{haystack});                                                                                                                      \
    }                                                                                                                                                                                      \
  }                                                                                                                                                                                        \
                                                                                                                                                                                           \
  namespace curry {                                                                                                                                                                        \
    template<typename cmp = std::equal_to<>, typename T>                                                                                                                                   \
    auto METHOD(T search) {                                                                                                                                                                \
      return [=](auto &haystack) {                                                                                                                                                         \
        return range::METHOD<cmp>(search, haystack);                                                                                                                                       \
      };                                                                                                                                                                                   \
    }                                                                                                                                                                                      \
  }

      WRAP_METHOD_NO_CONTEXT(naive)
      WRAP_METHOD_CONTEXT(borderArray)
      WRAP_METHOD_CONTEXT(knuthMorrisPratt)

#undef WRAP_METHOD_NO_CONTEXT
    }// namespace match
  }  // namespace patterns
}// namespace mst
