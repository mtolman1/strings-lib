#pragma once

#include <string>
#include <memory>

namespace mst {
  namespace patterns {
    namespace data_struct {
      template<typename T, typename Cmp = std::equal_to<>>
      class Trie {
        struct Node {
          bool terminator = false;
          std::unique_ptr<T> data = nullptr;
          std::shared_ptr<Node> sibling = nullptr;
          std::shared_ptr<Node> children = nullptr;
        };

        std::shared_ptr<Node> root = nullptr;
        Cmp cmp{};

      public:
        Trie() = default;

        template<typename Iter>
        Trie(const Iter& begin, const Iter& end) : Trie() {
          populate(begin, end);
        }

        template<typename Rng>
        Trie(const Rng& rng) : Trie() {
          if (std::is_same_v<std::decay_t<Rng>, char *>) {
            populate(std::begin(rng), std::prev(std::end(rng)));
          }
          else {
            populate(std::begin(rng), std::end(rng));
          }
        }

        template<typename Rng>
        Trie& add(const Rng& rng) noexcept {
          if (std::is_same_v<std::decay_t<Rng>, char*>) {
            // Remove trailing \0
            return add(std::begin(rng), std::prev(std::end(rng)));
          }
          else {
            return add(std::begin(rng), std::end(rng));
          }
        }

        template<typename Iter>
        Trie& add(const Iter& begin, const Iter& end) noexcept {
          if (!root) {
            populate(begin, end);
          }
          else {
            auto last = root;
            auto it = begin;
            for(; it != end; ++it) {
              auto next = findNext(last, *it, cmp);
              if (!next) {
                break;
              }
              last = next;
            }

            if (it == end) {
              last->terminator = true;
            }
            else {
              auto suffix = Trie(it, end);
              suffix.root->children->sibling = last->children;
              last->children = suffix.root->children;
            }
          }
          return *this;
        }

        template<typename Iter>
        bool contains(const Iter& begin, const Iter& end) const noexcept {
          auto n = getNode(begin, end);
          return n != nullptr && n->terminator;
        }

        template<typename Rng>
        bool contains(const Rng& range) const noexcept {
          if (std::is_same_v<std::decay_t<Rng>, char*>) {
            // Remove trailing \0
            return contains(std::begin(range), std::prev(std::end(range)));
          }
          else {
            return contains(std::begin(range), std::end(range));
          }
        }
      private:
        template<typename Iter>
        std::shared_ptr<Node> getNode(const Iter& begin, const Iter& end) const noexcept {
          if (root == nullptr) {
            return nullptr;
          }
          auto cur = root;
          auto it = begin;

          for(; it != end && cur != nullptr; cur = findNext(cur, *it, cmp), ++it) {}

          if (it == end) {
            return cur;
          }
          return nullptr;
        }

        template<typename Iter>
        void populate(const Iter& begin, const Iter& end) {
          root = std::make_shared<Node>();
          auto lastIt = root;
          for(auto it = begin; it != end; ++it) {
            auto newNode = std::make_shared<Node>();
            newNode->data = std::make_unique<T>(*it);
            newNode->terminator = false;
            lastIt->children = newNode;
            lastIt = newNode;
          }
          lastIt->terminator = true;
        }

        static std::shared_ptr<Node> findNext(const std::shared_ptr<Node>& n, const T& value, const Cmp& cmp) {
          if (n == nullptr) {
            return nullptr;
          }
          for (auto cur = n->children; cur != nullptr; cur = cur->sibling) {
            if (cmp(*(cur->data), value)) {
              return cur;
            }
          }
          return nullptr;
        }
      };

      template<typename Cmp = std::equal_to<>, typename Rng>
      auto trie(const Rng& rng) {
        return Trie<typename std::iterator_traits<decltype(std::begin(rng))>::value_type, Cmp>{rng};
      }

      template<typename Cmp = std::equal_to<>, typename Iter>
      auto trie(const Iter& begin, const Iter& end) {
        return Trie<typename std::iterator_traits<Iter>::value_type, Cmp>{begin, end};
      }
    }
  }
}
